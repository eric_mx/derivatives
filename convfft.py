from math import *
import numpy as np
import scipy as sp
from numpy.fft import *
from scipy import stats
from scipy.optimize import *
from scipy.integrate import quad,cumtrapz,trapz
from scipy.misc import derivative
import matplotlib.pyplot as plt
import matplotlib as mpl
import datetime
import QLbondcall
import swphelpers

from collections import defaultdict
from scipy.interpolate import UnivariateSpline

import QuantLib as ql


def qlswaption(today, ts,expiry,start,end,fixrate, a,sigma):

    fixed_leg_tenor = ql.Period(1, ql.Years)
    dc = ql.Actual360()
    fixed_leg_daycounter = ql.Actual360()
    floating_leg_daycounter = ql.Actual360()
    
    hyts = ql.RelinkableYieldTermStructureHandle(ts)
    t0_curve = ql.YieldTermStructureHandle(ts)
    index = ql.Euribor1Y(t0_curve)
    cal = index.fixingCalendar()
    start = cal.advance(ql.Date.todaysDate(), ql.Period(start,ql.Years))
    calldate = cal.advance(ql.Date.todaysDate(), ql.Period(expiry,ql.Years))
    #maturity = index.fixingDate(maturity)
   
    swap,fdates = swphelpers.makeSwap(today,start,
                  ql.Period(end,ql.Years),
                  1.,
                  fixrate,
                  index,t0_curve)

   
    calldates = [start] 
    swaption = swphelpers.makeSwaption(swap,calldates,ql.Settlement.Physical)
    volas = [ql.QuoteHandle(ql.SimpleQuote(sigma)),
             ql.QuoteHandle(ql.SimpleQuote(sigma))]
    meanRev = [ql.QuoteHandle(ql.SimpleQuote(a))]
    model = ql.Gsr(t0_curve, [today+100], volas, meanRev, 50.)
    
    engine = ql.DiscountingSwapEngine(hyts)
    swap.setPricingEngine(engine)
    swaptionEngine = ql.Gaussian1dSwaptionEngine(model)
    #swaptionEngine = ql.BlackSwaptionEngine(t0_curve,volas[0],index.dayCounter(),0.0)
    #swaptionEngine = ql.JamshidianSwaptionEngine(ql.HullWhite(t0_curve,a,sigma))
    swaption.setPricingEngine(swaptionEngine)
  
    return swap,swaption,model,index.dayCounter().yearFraction(today,calldate), fdates, swaptionEngine

def bondPrice(process,schedule,model):
    z = np.array([cumtrapz(process[:,i],schedule, initial=0.) for i in range(0,process.shape[1])]) 
    
    #print (np.mean(process,axis=1))
    #print ([model.alpha(t) for t in schedule])
    pr = np.exp(-z)
    return pr

def caplet(prices,times, X,exp,periodl):
    t0 = np.where(times == exp)
    t1 = np.where(times == (exp+periodl))

    strikes = prices[...,t0[0][0]]*X
    spots = prices[...,t1[0][0]]
    
    df=strikes-spots
    df = df[df > 0]
    return np.mean(df)
    
    


def plot_val_difference(vtype='int'):
    k_list=np.linspace(S0*0.8,S0*1.2,50)
    ana_values=BSM(S0, k_list, T, r, sigma)
    plt.figure(figsize=(8,6))
    plt.subplot(211)
    plt.plot(k_list,ana_values,'b',label='analytical',lw=1.5)
    
    int_values=np.array([FFTConv_E(n,L,-1.,1,S0,T,r,0.,K,sigma) for K in k_list])
    plt.plot(k_list,int_values,'r-.',label='Fourier Convolution',lw=1.5)
    diffscv = np.abs(int_values - ana_values)
    rdiffscv = diffscv/ana_values
    
    fft_values = np.array([BSM_fft(S0, K, T, r, sigma) for K in k_list])
    plt.plot(k_list,fft_values,'g-.',label='Fourier CarrMadan',lw=1.5)
    diffscm = np.abs(fft_values - ana_values)
    rdiffscm = diffscm/ana_values

    plt.legend()
    plt.grid()
    plt.subplot(212)
    plt.plot(k_list,rdiffscv,'g',label='rel.difference CV',lw=1.5)
    plt.legend(loc=0)
    plt.grid()
    plt.plot(k_list,rdiffscm,'r',label='rel.difference CM',lw=1.5)
    plt.legend(loc=0)
    plt.grid()    
    
    
    #plt.subplot(313)
    #plt.plot(k_list,rdiffs,'r',label='relat. difference',lw=1.5)
    #plt.legend(loc=0)
    plt.xlabel('strike')
    #plt.grid()
    plt.tight_layout()




class QLTest:
    def __init__(self,A,SIGMA,FWD,mat):
        self._today = ql.Date.todaysDate()
        self._sigma = SIGMA
        self._a = A
        self._forward_rate=FWD
        self._day_count = ql.Thirty360()
        ql.Settings.instance().evaluationDate = self._today
        self._spot_curve = ql.FlatForward(self._today, ql.QuoteHandle(ql.SimpleQuote(self._forward_rate)), self._day_count)
        self._spot_curve_handle = ql.YieldTermStructureHandle(self._spot_curve)
        self._length = mat #years
        self._timestep = 12*self._length
        self._arr = None 
        self._time = None
        self._b = None
        
    def samples(self,N=1000):
        hw_process = ql.HullWhiteProcess(self._spot_curve_handle, self._a,
                                         self._sigma)
        rng = ql.GaussianRandomSequenceGenerator(ql.UniformRandomSequenceGenerator(self._timestep, ql.UniformRandomGenerator()))
        seq = ql.GaussianPathGenerator(hw_process, self._length, self._timestep, rng, False)        
        self._arr = np.zeros((N, self._timestep+1))
        for i in range(N):
            sample_path = seq.next()
            path = sample_path.value()
            time = [path.time(j) for j in range(len(path))]
            value = [path[j] for j in range(len(path))]
            self._arr[i, :] = np.array(value)
            self._time = np.array(time)
            
        self._b = np.mean(np.exp(-cumtrapz(self._arr,self._time,initial=0.)),axis=0)
        return self._time
       
        
        
#todo convert dates 30/360
class IRCurve:
    def __init__(self,start_date,nperiod,period, poffset=0.5):
        self._rates=defaultdict()
        self._start_date=np.datetime64(start_date)
        self._interp = None
        self._nkeys = None
        self._skeys = None
        self._rebuildf = True
        self._defaultrate = -1
        self._period = np.timedelta64(nperiod,period) 
        self._delta = self._period/np.timedelta64(1,'Y')
        self._poffset = poffset
        
        
    def set(self,pi, value):
        dT  = pi*self._period/np.timedelta64(1,'Y')
        self._rates[dT]=value
        self._rebuildf=True
        self._defaultrate = value
 
    #finmath implem (= not efficient)
    def zcp(self,T):
        DF = 1.
        dd = 0
        for (t0,t1) in self.iterator(0,T):
            r = self.get_fwd(t1)
            dd += r*(t1-t0)
            DF /= (1+r*(t1-t0))
               
        return exp(-dd)
        #t = 0
        #while t < T:
        #    DF /= 1 + self.get_fwd(t)*(min(self._poffset,T-t))
        #    t += self._poffset
        #return DF
  
    def _rebuild(self):
        if self._rebuildf:
            self._nvalues = np.fromiter(self._rates.values(),dtype=float)
            self._nkeys = np.fromiter(self._rates.keys(),dtype=float)
            self._skeys = np.argsort(self._nkeys)
            self._rebuildf = False 
     
    def dt_iterator(self,start,stop,ds=1):
        pdelta = self._period
        st_=start
        sp_=stop
        for d in np.arange(st_,sp_+ds,1.):
            yield d
     
    def iterator(self,start,stop):
        l = list(np.arange(start,stop, self._delta))
        l.append(stop)
        return zip(l[:-1],l[1:])
    
    def fracP(self,period):
        return np.timedelta64(period[0],period[1])/self._period
    def get_rate(self,dT):
        self._rebuild()    
        return np.interp(dT, self._nkeys[self._skeys],self._nvalues[self._skeys])
    
    def get_df_obsolete(self,t):
        self._rebuild()
        dfs = self.get_rates(t)
        return np.interp(t, dfs[2],dfs[0])
    
    def zcp_(self,t,T):
        fwd = self.get_rate(T)
        return exp(-fwd*(T-t))
        #return 1/(1+fwd*(T-t))
    
    def get_fwd(self,t):
        dfs = self.get_rates(t)
        return np.interp(t, dfs[2],dfs[1])
    
    def get_rates(self,T,shift=('0','Y')):
        self._rebuild()
        r =[]
        f =[]
        lastDF=1.
        pdT = 0
        ts = []
        for dT in self.dt_iterator(self.fracP(shift),T*self.fracP((1,'Y')),ds=1.1):
            fwd = self.get_rate(dT)
            lastDF /= (1 + fwd*(dT-pdT))
            pdT = dT
            ts.append(dT)
            r.append(lastDF)
            f.append(fwd)
            
        return np.array(r),np.array(f),np.array(ts)
   
    def get_NPV(self,nominal,T,fixrate,shift=('0','Y')):
        self._rebuild()
              
        df,f = self.get_rates(T,shift)
        FV = nominal*(f-fixrate)*self._period
        dT = 0
        fvp = [0]
        i = 1
        for dT in self.dt_iterator(self.fracP(shift),T*self.fracP((1,'Y')),ds=1.):
            fvp.append(FV[i]*df[i]/df[-1])
            i+=1
        v = [fvp[i]*df[i] for i in range(0,len(df)-1)]
        return sum(v)
        
class Option:
    def __init__(self,cp,K,S,T=0):
        #riskfree rate
        #vol
        #dividends        
        self._cp = cp #call 1 put -1
        self._K = K
        self._T = T
        self._S = S
        self._type='E'
       

class Pricer:
    def _fmax(self,x):
        return x if x > 0. else 0.
   
    def __init__(self,n=12,delta=10.,alpha=-0.6):
        self._alpha = alpha
           
        self._n =n #N=2**n FFT granularity
        self._delta = delta
        self._FMX = np.vectorize(self._fmax)    
     
    def Compute(self,model,option,S0,n):
        N=pow(2,n)
        rdt = model._r*option._T
        
        L = 10.
        delta_y = L/N
        delta_x = delta_y
        delta_u = 2*pi/L
    
        grid_i = np.arange(0,N,1)
        grid_m = np.power(-1,grid_i)
    
        x = (grid_i*delta_x) - (N/2*delta_x)
        y = log(option._K / S0) + (grid_i * delta_y) - (N/2 * delta_y)
        u = (grid_i * delta_u) - (N/2 * delta_u)
        rw = option._cp * (S0*np.exp(y) - option._K)
        def fmax(x):
            return x if x > 0. else 0.
        FMX = np.vectorize(fmax)
        V = FMX(rw)
        v = V * np.exp(self._alpha * y)
    
        w = np.ones((int(N),))
        w[0] = w[-1]=0.5
        FT_Vec = ifft( (grid_m) * w * v)
        alp = -(u - (1j*self._alpha))
        #bs = model.psi_bs(alp,option._T)
        
        bs = model.lnphi_zc(alp,option._T,option._S)
        FT_Vec_tbt = np.exp( 1j * grid_i * (y[0] - x[0]) * delta_u) * np.exp(bs) * FT_Vec

        C = abs(np.exp( -rdt - (self._alpha * x) + (1j * u * (y[0] - x[0])) )*(grid_m)*fft(FT_Vec_tbt))
        return C[int(N/2)]   
        
class SwaptionPricer(Pricer):
    def __init__(self, model, n=12, delta=10., alpha=-1.1):
        self._model = model
        self._alpha=alpha
        self._delta = delta
        self._n = n
        self._r = -1
        self.fairRate=0
         
    def Compute(self,N,X,T,Ts0,Ts1):
        self._r = self._model.r_star(T,Ts0,Ts1)
        #self._r = swaprate
        s = 0
        for ti_1,ti in zip(Ts0,Ts1):
            ci = X*(ti-ti_1)
            Xi = self._model.discount_bond(T,ti,self._r)
            zi = self.zbp(T,ti,Xi)
            zi_ = self._model.zbp(0,T,ti,Xi)
            #print ('Fourier zbp',zi,'t=',T,'s=',ti)
            s += (ci*zi)
            ti_1 = ti
        s += zi
        return N*s
  
    def zbp(self,T,ti,Xi):
        N=pow(2,self._n)
        rdt = 0.
        L = 10.
        strike = self._model.zcp(0,T)*Xi
        spot = self._model.zcp(0,ti)
    

        delta_y = L/N
        delta_x = delta_y
        delta_u = 2*pi/L

        grid_i = np.arange(0,N,1)
        grid_m = np.power(-1,grid_i)

        x = (grid_i*delta_x) - (N/2*delta_x)
        y = log(strike/ spot) + (grid_i * delta_y) - (N/2 * delta_y)
        u = (grid_i * delta_u) - (N/2 * delta_u)
        cp = -1.
        rw = cp * (spot*np.exp(y) - strike)
        def fmax(x):
            return x if x > 0. else 0.
        FMX = np.vectorize(fmax)
        V = FMX(rw)
        v = V * np.exp(self._alpha * y)
        w = np.ones((int(N),))
        w[0] = w[-1]=0.5
        FT_Vec = ifft( (grid_m) * w * v)
        alp = -(u - (1j*self._alpha))
        bs = self._model.lnphi_zc(alp,T,ti,Xi)
        FT_Vec_tbt = np.exp( 1j * grid_i * (y[0] - x[0]) * delta_u) * np.exp(bs) * FT_Vec
        C = abs(np.exp( -rdt - (self._alpha * x) + (1j * u * (y[0] - x[0])) )*(grid_m)*fft(FT_Vec_tbt))
        return C[int(N/2)]       
class Model:
    def __init__():
        pass
    def psi(self,u):
        pass
    
class BlackScholes(Model):
    def __init__(self,ircurve,d,sigma):
        self._sr = ircurve
        self._d = d
        self._sigma = sigma
    
    def lnphi(self,u,T):
        return 1j*u*((self._r-self._d-0.5*self._sigma**2)*T) - 0.5*u*u*T*self._sigma**2
    
    def phi(self,u,T,alpha):
        return self.lnphi(v-(alpha+1)*1j,T)/(alpha**2 + alpha -v**2 + 1j*(2*alpha + 1)*v)
    def ps(self,t,N,X,T,Ts):
        #X: fixed rate
        s = 0
        irc = self._sr
        
        for ti_1,ti in zip(Ts[0],Ts[1]):
            ci = X*(ti-ti_1)
            Xi = irc.zcp(T,ti)
            zi = self.zbp(t,T,ti,Xi)
            s += (ci*zi)
            ti_1 = ti
        s += zi
        return N*s    
    def zbp(self,t,T,S,X):
        irc = self._sr
        p_t_S = irc.zcp(t,S)
        p_t_T = irc.zcp(t,T)
        if T >= S:
            return max(0,X*p_t_T-p_t_S)
        sigma = self._sigma
        d1 = (log(p_t_S/(X*p_t_T))+0.5*sigma**2*T)/(sigma*sqrt(T))
        d2 = d1 - sigma*sqrt(T)
        C =  X*p_t_T*stats.norm.cdf(-d2) - p_t_S*stats.norm.cdf(-d1,0.,1.)
        #print ('spot',p_t_S,'strike',X*p_t_T, 'sigma',sigma,'T:',T,'S:',S,'put price:',C)
        return C    
    
class HullWhite(Model):
   
    def __init__(self,A, varf,ircurve,r):
        self._sigma = varf
        self._a = A
        self._sr = ircurve
        self._r = r
        self._d = 0.
        self.DCOUNT=365
        self.caplet=True
    
    def r_star(self,T0,Tn0,Tn1):
        fwdrate = self.r_(T0)
        def fun(c):
            return (abs(1 - self.checksps(fwdrate,T0,Tn0,Tn1,c)))
        rr = root(fun,fwdrate,method='lm',options={'xtol':1e-9})
        #print('r*',rr.x[0])
        return rr.x[0]
    
    #-d log(P)/dt
    
    def f_M(self,t):
        def fl1(x):
            return self._sr.zcp_(0,x)
        def fl2(x):
            return log(self._sr.zcp_(0,x))  
        e = 1e-9
        d1= -derivative(fl2,t)
        d2 = -(fl1(t+e) - fl1(t))/(e*fl1(t))
        
        return d1    
    
    def f_M2(self,t):
        def fl(x):
            return log(self._sr.zcp(x))
        
        return -derivative(fl,t)
    
    def psi_bs(self,u,T):
        return 1j*u*((self._r-self._d-0.5*self._sigma*self._sigma)*T) - 0.5*self._sigma*self._sigma*u*u*T  
    
    def sigma_f(self,t):
        el=exp(-self._lambda*t)
        return (self._eta**2 / self._lambda**2)*(t + (el*(2.0 - 0.5*el) -1.5)/self._lambda)
            
    
    def V(self,t,T):
        return self._sigma**2/self._a**2 *(T-t + 2/self._a*exp(-self._a*(T-t)) -.5/self._a*exp(-2*self._a*(T-t)) -1.5/self._a)
    
    
    def expintR(self,t,T):
        return log(self._sr.zcp(t)/self._sr.zcp(T)) + .5*(self.V(0,T)-self.V(0,t)) + self._B(t,T)*(self.r_(t)-self.alpha(t))
    def r_(self,t):
        return (self._sr.get_rate(0) - self.alpha(0))*exp(-self._a*t) + self.alpha(t)
        #return self._sr.get_rate(t)  + self.alpha(t)
        
        #date_T = self._sr._start_date + np.timedelta64(int(self.DCOUNT*t),'D')
        
    def lnphi_old(self,u,t,T):   
        tau_0_T = T
        tau_0_t =  t
        tau_t_T = T-t
        delta=1e-9
        p0_T = self._sr.zcp(0,T)
        p0_t = self._sr.zcp(0,t)
        p0_t_dt = self._sr.zcp(0,tau_0_t + delta)
        instR = (1.0/p0_t_dt-1.)/(tau_0_t+delta)
        instF = -(log(p0_t_dt)-log(p0_t))/(delta)/p0_t
        
        b_t_T = (1-exp(-self._lambda*tau_t_T))/self._lambda
        psi_T = instF + .5*(self._eta**2 / self._lambda**2)*(1-exp(-self._lambda*tau_0_t))**2
        sigma2R = self.sigma_f(tau_t_T)
        mu_R = b_t_T*(instR - psi_T) + 0.5*(self.sigma_f(tau_0_T) - self.sigma_f(tau_0_t)) +log(p0_t/p0_T) 
        return 1j*u*mu_R - 0.5*u*u*sigma2R
    
    def alpha(self,t):
        return self.f_M(t) + .5*((self._sigma/self._a) * (1-exp(-self._a*t)))**2
   
    def sigma_t(self,T,S):
        return self._sigma*sqrt((1-exp(-2*self._a*T))/(2*self._a))*self._B(T,S)
    def h_(self,T,S,X):
        st = self.sigma_t(T,S)
        return 1/st*log(self.zcp(0,S)/(self.zcp(0,T)*X)) + st/2.
    
    def lnphi_zc(self,u,t,T,X):   
        #sigma2 = self.sigma_f(T-t)
        sigma2 = self.sigma_t(t,T)
        
        return -1j*u*(0.5*self._sigma**2) - 0.5*u*u*sigma2*sigma2
    def strike(self,T0,ti):
        sdk=0
        A = 0
        t0 = 0
        for t in ti:
            di = self.zcp(0, t)
            di_ = exp(-self.r_(t)*t)
            sdk += di
            A += di*(t-t0)
            t0 = t
            
        d_n = di# exp(-r*ti[-1])
        Is = (1-d_n)/sdk        
        return Is
        
    def lnphi_zc__(self,u,t,T):
        sigma2 = self.sigma_f(T-t)
        #mu_R = self.exp_zc(t, T)
        mu_R = 0.
        print ('Fourier sigma2',sigma2,'exp',mu_R)
        return -1j*u*mu_R - 0.5*u*u*sigma2   
    
    def zcp(self,t,T,r=-1):
        if r == -1:
            r = self.r_(t)
        B_t_T = self._B(t,T)
        E_ = B_t_T*(self.f_M(t) -  r) - self._sigma**2/(4*self._a)*(1-exp(-2*self._a*T))*B_t_T**2
     
        return self._sr.zcp(T)/self._sr.zcp(t)*exp(E_)        

    def _B(self,t,T):
        return 1/self._a*(1-exp(-self._a*(T-t)))
    
    def _A(self,t,T):
        B_t_T = self._B(t,T)
        E_ = B_t_T*self.f_M(t) - self._sigma**2/(4*self._a)*(1-exp(-2*self._a*T))*B_t_T**2
        return self._sr.zcp(T)/self._sr.zcp(t)*exp(E_)
        
    def zbp(self,t,T,S,X):
        p_t_S = self.zcp(t,S)
        p_t_T = self.zcp(t,T)
        if T >= S:
            return max(0,X*p_t_T-p_t_S)        
        
        sig_p = self._sigma*sqrt((1-exp(-2*self._a*(T-t)))/(2*self._a)) * self._B(T,S)
        h = 1/sig_p*log(p_t_S/(X*p_t_T)) + sig_p/2
        C =  X*p_t_T*stats.norm.cdf(-h+sig_p,0.,1.) - p_t_S*stats.norm.cdf(-h,0.,1.)
        #print (f'd1 :{-h+sig_p},d2:{-h}')
        print ('spot',p_t_S,'strike',X*p_t_T, 'sigma',sig_p,'T:',T,'S:',S,'put price:',C)
        return C
    
   
    def ps(self,t,N,X,T,Ts0,Ts1,pct=False):
        #X: fixed rate
        s = 0
        irc = self._sr
        #if not self.caplet:
        r_star = self.r_star(T,Ts0,Ts1)
        #r_star = X
        #print (f'r_star={r_star}')
        self._r = r_star
        
        for ti_1,ti in zip(Ts0,Ts1):
            ci = X*(ti-ti_1)
            Xi = self.discount_bond(T,ti,r_star)
            zi = self.zbp(t,T,ti,Xi)
            if pct : 
                zi /= Xi
                zi *= 100.
            s += (ci*zi)
            ti_1 = ti
        s += zi
        return N*s
   
    def checksps(self,X,T,Ts0,Ts1,r_star):
        #X: fixed rate
        s = 0
        irc = self._sr
        for ti_1,ti in zip(Ts0,Ts1):
            ci = X*(ti-ti_1)
            pr_i = self.discount_bond(T, ti, r_star)
            s += (ci*pr_i)
            ti_1 = ti
        s += pr_i
        return s 
    def checksps_(self,X,T,Ts0,Ts1,r_star):
        #X: fixed rate
        s = 0
        irc = self._sr
    
        for ti_1,ti in zip(Ts0,Ts1):
            ci = X*(ti-ti_1)
            pr_i = irc.discount_bond(T, ti, r_star)
            s += (ci*pr_i)
            ti_1 = ti
        s += pr_i
        return s     
    def var(self,t,T):
        v =  (T - t + 2./self._a*exp(-self._a*(T-t)) - 0.5/self._a*exp(-2*self._a*(T-t)) - 1.5/self._a)
        return v*(self._sigma/self._a)**2
    
    def sigma_zc(self,T,S):
        return self._sigma*sqrt((1-exp(-2*self._a*(T)))/(2*self._a)) * self._B(T,S)

    def varbachelier(self,T,S):
        return self.var(T,S)/pow(S-T,1.5)
    
    def exp_zc(self,T,t):    
        p1 = (self.r_(t) - self.alpha(0))*exp(-self._a*(T-t))
        p2 = self.alpha(t) - self.M_(T, t, 0)
        return p1 - p2
    
    def varHW(self,t):
        return self._sigma**2/(2*self._a)*(1-exp(-2*self._a*t))
    
    
    def process1(self,nsteps,t,T,nsim=1000):
        Tdt = np.array([t + (T-t)*i/nsteps for i in  range(0,nsteps)])
        
        v = np.vectorize(self.varHW)(Tdt)
        e = np.vectorize(self.alpha)(Tdt)
        return np.array([np.random.normal(e[t],v[t],nsim) for t in range(0,nsteps)]),Tdt
             
          
    def MCzcp(self,t,T):
        nsteps = max(360,int(T-t)*360)
        Tdt = np.array([t + (T-t)*i/nsteps for i in  range(0,nsteps)])
        v = np.vectorize(self.varHW)(Tdt)
        e = np.vectorize(self.alpha)(Tdt)        
        l = [np.random.normal(e[t],v[t],1)[0] for t in range(0,nsteps)]
        p = trapz(l,Tdt)
        return exp(-p)
        
        
        
    def M_(self,T,s,t):
        p1 = (self._sigma/self._a)**2*(1-exp(-self._a*(t-s)))
        p2 = 0.5*(self._sigma/self._a)**2 * (exp(-self._a*(T-t)) - exp(-self._a*(T+t-2*s)))
        return p1 - p2
    
    def discount_bond(self,t,T,r):
        return self._A(t,T)*exp(-self._B(t, T)*r)



class Swap:
    def __init__(self,notional,fix, irc,model,fixsch, floatsch, poffset=0.5):
        self._model = model
        self._notional = notional
        self._irc = irc
        self._fixsch = fixsch
        self._floatsch = floatsch
        self._poffset = poffset
        self._fix = fix
        
        
    #finmath implem (= not efficient)
    def zcpfix(self, T):
        t = 0
        DF = 1.
        while t < T:
            DF /= 1 + self._fix*(min(self._poffset,T-t))
            t += self._poffset
        return DF
    
    def npvfloat(self):
        f = 0
        for ti,ti_n in zip(self._fixsch[:-1],self._fixsch[1:]):
            Di = self._irc.zcp(ti_n)
            f += (Di*(ti_n-ti))*self._irc.get_fwd(ti)        
        return f 
    
    def fwdSwapRate(self,T):
        A = self.annuity(T)
        return A, (self.npvfloat()/self._irc.zcp(T))/A
    
    def annuity(self,T):
        s=0
        for ti_1,ti in zip(self._fixsch[:-1],self._fixsch[1:]):
            if ti < T :
                continue
            Di = self.zcpfix(ti)
            s += (Di*(ti-ti_1))
        return s / self.zcpfix(T)
    
def psi_bs(u,T,r,d,sigma):
    return 1j*u*((r-d-0.5*sigma*sigma)*T) - 0.5*sigma*sigma*u*u*T

def psi(v,alpha,T,r,d,sigma):
    return np.exp(psi_bs(v-(alpha+1)*1j,T, r, d, sigma))/(alpha**2 + alpha -v**2 + 1j*(2*alpha + 1)*v)

def FFT_Carr(n,alpha,S0,t,r,strike,sigma):
    lnS = log(S0)
    lnK = log(strike)
    kappa=lnS-lnK
    DF = exp(-r*T)
    N = pow(2,n)
    eta = 0.05
    lambd = (2*pi)/(N/eta)
    b = (N*lambd)/2
    uvec = np.arange(0,N,1)
    k_u = - b + lambd*uvec
    jvec = np.arange(0,N,1)
    v_j = jvec*eta
    tmp = DF * psi(v_j, alpha, T, r, 0, sigma) * np.exp(1j*v_j * b)*eta
    #simpson rule
    tmp = (tmp/3)*(3+np.power(-1,jvec - ((jvec-1)==0)))
    cpvec = (np.exp(-alpha*k_u)*fft(tmp)/pi).real
    index = (np.floor((lnK + b)/lambd +1)).astype(int)
    iset = np.arange(min(index)-1,max(index)+1,1)
    xp = k_u[iset]
    yp = cpvec[iset]
    callp = np.interp(lnK,xp,yp)
    return callp

def f_hat(v,x0,T,r,sigma):
    cfv = np.exp(((x0/T + r -0.5*sigma**2)*1j*v-0.5*sigma**2*v**2)*T)
    return cfv
def BSM_fft(S0,K,T,r,sigma,pc=1,percent=False):
    k = np.log(K/S0)
    x0 = np.log(S0/S0)
    g=1
    N = g*4096
    eps=(g*150)**-1
    eta=2*np.pi / (N*eps)
    b = 0.5*N*eps - k
    u = np.arange(1,N+1,1)
    v0=eta*(u-1)
    
    if S0 >= 0.95*K:
        alpha=1.5
        v = v0 - alpha*1j -1j
        modF = np.exp(-r*T)*(f_hat(v, x0, T, r, sigma)/(alpha**2 + alpha - v0**2+ 1j*(2*alpha+1)*v0))
    else:
        alpha=1.1
        v = v0 - alpha*1j -1j
        modF1=np.exp(-r*T)*(1/(1+1j*(v0-1j*alpha))
                            - np.exp(r*T)/(1j*(v0-1j*alpha))
                            - f_hat(v,x0, T, r, sigma)/((v0-1j*alpha)**2 -1j*(v0-1j*alpha)))
        
       
        v = v0 + alpha*1j -1j
        modF2=np.exp(-r*T)*(1/(1+1j*(v0+1j*alpha))
                            -np.exp(r*T)/(1j*(v0+1j*alpha))
                            - f_hat(v,x0, T, r, sigma)/((v0+1j*alpha)**2 - 1j*(v0+1j*alpha))) 
        
        
    
    delt = np.zeros(N,dtype=np.float)
    delt[0]=1
    j = np.arange(1,N+1,1)
    SimpsonW = (3 + (-1)**j-delt)/3
    if S0 >= 0.95*K:
        FFTf = np.exp(1j*b*v0)*modF*eta*SimpsonW
        payoff=fft(FFTf).real
        callValueM=np.exp(-alpha*k)/np.pi*payoff
    else:
        FFTf=(np.exp(1j*b*v0)*(modF1-modF2)*0.5*eta*SimpsonW)
        payoff=fft(FFTf).real
        callValueM=payoff / (np.sinh(alpha*k)*np.pi)   
   
    pos = int((k+b)/eps)
    call_value = callValueM[pos]*S0
    if percent:
        call_value /= K*100.
    return call_value

def FFTConv_E(n,L,alpha,cp,S0,t,r,q,strike,sigma):
    N=pow(2,n)
    rdt = r*t
    delta_y = L/N
    delta_x = delta_y
    delta_u = 2*pi/L
    
    grid_i = np.arange(0,N,1)
    grid_m = np.power(-1,grid_i)
    
    x = (grid_i*delta_x) - (N/2*delta_x)
    y = log(strike / S0) + (grid_i * delta_y) - (N/2 * delta_y)
    u = (grid_i * delta_u) - (N/2 * delta_u)
    rw = cp * (S0*np.exp(y) - strike)
    def fmax(x):
        return x if x > 0. else 0.
    
    FMX = np.vectorize(fmax)
    V = FMX(rw)
    v = V * np.exp(alpha * y)
    
    w = np.ones((int(N),))
    w[0] = w[-1]=0.5
    FT_Vec = ifft( (grid_m) * w * v)
    alp = -(u - (1j*alpha))
    bs = psi_bs(alp,t,r,q,sigma)
    FT_Vec_tbt = np.exp( 1j * grid_i * (y[0] - x[0]) * delta_u) * np.exp(bs) * FT_Vec
    C = abs(np.exp( -rdt - (alpha * x) + (1j * u * (y[0] - x[0])) )*(grid_m)*fft(FT_Vec_tbt))
    price = C[int(N/2)]   
    return price
    
def BSM(S0,K,T,r,sigma,pc=1):
    d1 = (np.log(S0/K) + (r + 0.5*sigma**2)*T)/(sigma*np.sqrt(T))
    d2 = (np.log(S0/K) + (r - 0.5*sigma**2)*T)/(sigma*np.sqrt(T))
    C = pc*S0*stats.norm.cdf(pc*d1,0.,1.) - pc*K*np.exp(-r*T)*stats.norm.cdf(pc*d2,0.,1.)
    return C
def testBSM():
    alpha = -0.6
    cp = 1
    S0 = 90.
    K = 100.
    T = 1.
    r = 0.05
    sigma = 0.4
    q=0.
    n = 16
    delta = 10
    
    p2 = BSM(S0,K,T,r,sigma)
    return p2


def plot_val_difference():
    k_list=np.linspace(s0*0.6,s0*1.4,50)
    ana_values=BSM(s0, k_list, T, r, sigma)
    plt.figure(figsize=(8,6))
    plt.subplot(311)
    plt.plot(k_list,ana_values,'b',label='analytical',lw=1.5)
   
    int_values=np.array([FFTConv_E(n,L,alpha,cp,s0,T,r,q,K) for K in k_list])
    plt.plot(k_list,int_values,'r-.',label='Fourier (convolution)',lw=1.5)
    diffs = np.abs(int_values - ana_values)
    rdiffs = diffs/ana_values

    plt.legend()
    plt.grid()
    plt.subplot(312)
    plt.plot(k_list,diffs,'g',label='abs.difference',lw=1.5)
    plt.legend(loc=0)
    plt.grid()
    plt.subplot(313)
    plt.plot(k_list,rdiffs,'r',label='relat. difference',lw=1.5)
    plt.legend(loc=0)
    plt.xlabel('strike')
    plt.grid()
    plt.tight_layout()

#plot_val_difference()
#plt.show()
def f(t,a,sigma):
    l = a
    eta = sigma
    s = eta**2/l**2
    s = s*(t + 2/l*exp(-l*t) -0.5/l * exp(-2*l*t) -1.5/l)
    return s
 
def f2(t,e,l):
    s = e**2/l*(1-exp(-2*l*t))
    return s

def ftest3(ch,hw):
    x1 = hw.zcp(1,2,(ch.x)[0])
    pzp1 = hw.zbp(0,1,2,x1)

    x2 = hw.zcp(1,3,(ch.x)[0])
    pzp2 = hw.zbp(0,1,3,x2)
    tt = pzp1*r + pzp2*(1+r)
    spot =  0.9048374157235807
    strike = 0.9101140773337103
    sig = 0.098024768397558
    psw = BSM(spot,strike,1,0.0,sig,-1.)
    print (psw)

    p2 = FFTConv_E(10,10.,1.1,-1,spot,1,0.,0.,strike,sig)
    print (p2)    

    
def testbonds():
    T= 1.
    r = 0.05
    n = 8
    delta = 100
    #L = delta*SIGMA*sqrt(T)
    alpha = 1.1
    cp = 1
    q=0
    Mat=20
    SIGMA=0.1
    A = 0.05
    FWD=0.05
    
    irc = IRCurve(datetime.datetime.now(),Mat,'Y')
    
    irc.set(1, FWD)
    irc.set(5,FWD)
    irc.set(10, FWD)
    irc.set(20, FWD)
    irc._rebuild()
    qlt = QLTest(A,SIGMA,FWD,Mat)
    hw = HullWhite(A, SIGMA, irc,r)    
    hwm = ql.HullWhite(qlt._spot_curve_handle, qlt._a,qlt._sigma)
    hwmzc = np.vectorize(hwm.discount)
    ts_handle,bond = QLbondcall.callbond(1,r)
    QLbondcall.value_bond(A, SIGMA, 40, bond,ts_handle)
    print ("Bond price: ",bond.cleanPrice())    
 
    for c in bond.cashflows():
        print (c.date(), "     ", c.amount() )  
    
   
    times = qlt.samples(10000) 
    plt.plot(times,
             np.vectorize(hwm.discount)(times),"r-.", 
             label="HW(QL)", lw=2)
    #plt.plot(times,
    #         qlt._b,"r-.", 
    #         label="MonteCarlo(QL)", lw=2)    
    plt.plot(times,
             np.vectorize(lambda T: hw.zcp(0,T,r))(times),"g:",
             label="Model", lw=2)
    
    plt.title("Zero Coupon Bond Price")
    plt.legend() 
    plt.show()
def testswaption():
    SIGMA = 0.04
    A = 0.01
    r = 0.05
    fx = 0.05
    Mat = 2
    N= 10
    irc = IRCurve(datetime.datetime.now(),1,'Y')
    irc.set(1, r)
    irc.set(5,r)
    irc.set(10, r)
    irc.set(20,r)
    
    today = ql.Date.todaysDate()
    hw = HullWhite(A, SIGMA, irc,fx)
    p1 = hw.process1(360,0,10,1001)
     
    spots = bondPrice(*p1,hw)
    TTT = p1[1]    
    for exercise in range(1,20):
    
        qlt = QLTest(A, SIGMA,r,Mat)
        ts = swphelpers.getRateCurve1(today,r)
        swap,swaption,model,swapmat, fdates,engine = qlswaption(today, ts,1,1,1,fx,A,SIGMA)
        s1_1 = quad(lambda t: hw.r_(t),0.0,1.)
        s1_5 = quad(lambda t: hw.r_(t),0.0,0.5)
        s01  = hw.zcp(0,1) 
        
        aa  = hw.expintR(0,1)
        aa_ = hw._A(0,1)
        
        bl = BlackScholes(irc,0.,SIGMA)
        pr2 = swaption.NPV()
        #swapmat = exercise*0.5
        
        #pr2 = hw.zcp(0,swapmat + 2,hw.alpha(swapmat+2))
        
        #fdates = ([swapmat + i*.5 for i in range(0,1)],[swapmat + (i+1)*.5 for i in range(0,1)])
        #fxsched = [0.5]
        
        swap = Swap(1.,r,irc,hw,[swapmat,swapmat + 0.5], [swapmat,swapmat + 0.5])
        ann,swr = swap.fwdSwapRate(swapmat)
        bondp = hw._sr.zcp(swapmat)
       
       
       
        #cpl = caplet(spots,TTT, irc.zcp(swapmat),swapmat,0.5)
        #pr4,vol = swaptionBachelier(hw,swapmat,fdates,fx,fx)
        pr = hw.ps(0,1,fx,swapmat,*fdates,False)
        
        pricer = SwaptionPricer(hw)
        #
        #r_star = hw.r_star(swapmat,*fdates)
        #pricer.fairRate = r_star
        pfft = pricer.Compute(1.,fx, swapmat,*fdates)
        
        
        #caplet = BSM_fft(S0,K,T,r,sigma,pc=1,percent=True)
        print (f'mat:{swapmat} caplet :{pr*100.} vol {hw.sigma_zc(swapmat,swapmat+0.5)/sqrt(swapmat)}')


def swaptionBachelier(model,T,Ts, X,F):
    
    sdk=0
    s = 0
    irc = model._sr
    t0 = T
    A = irc.annuity(T,Ts)
    A_ = model.annuity(T,Ts,F)
   
    sigma = model.sigma_t(T)
    if (X == F):
        return sigma*sqrt(T/pi/2.)*A,sigma
  
    d1 = (F-X)/(sigma * sqrt(T))
   
    
    return  A * ((F-X) * stats.norm.cdf(d1) +  sigma*sqrt(T) * stats.norm.cdf(-d1))

#testbonds()
testswaption()


irc = IRCurve(datetime.datetime.now(),6,'M')
irc.set(1, 0.05)
irc.set(4, 0.04)


#a = irc.zcp(0.5)
#b = irc.zcp(1.1)
#c = irc.zcp(4.4)
#T = np.arange(0,10,0.1)
#hw2 = HullWhite(0.001,0.01,irc,0.05)
#v = np.vectorize(lambda t : -log(irc.zcp(t)))(T)
#v2 = np.vectorize(hw2.f_M)(T)

#plt.plot(T,v,'r-.')
#plt.plot(T,v2,'g-.')

#plt.show()