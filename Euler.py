import numpy as np
import matplotlib.pyplot as plt
from math import *
from scipy import interpolate
from scipy.misc import derivative

import datetime
import convfft

class HWProcess:
    def __init__(self,a, sigma,zcp,r0):
        self._sigma = sigma
        self._a = a
        self.zcp = zcp
        self._e = 1e-10
        self.r0 = r0
    
    def f_M(self,t):
        def fl1(x):
            return self.zcp(x)
        def fl2(x):
            return log(fl3(x))  
        def fl3(x):
            return (x*pow((1-x),2))*.05 + 0.05
        d1=  (fl3(t+self._e) - fl3(t-self._e))/(2*self._e*fl3(t))
        d2 = (fl3(t+self._e) - fl3(t))/(self._e*fl3(t))
        #return 0.05*sqrt(t)
        return d2
    
    def lnP(self,t):
        return -log(self.zcp(t))
    def mu(self,y,t):
        return  - self._a*y
    
    def sigma(self,y,t):
        return self._sigma
    
    
    def alpha(self,t):         
        #return  self.alpha0(t) + self.f_M(t) + .5*((self._sigma/self._a) * (1-exp(-self._a*t)))**2
        return self.f_M(t) + .5*((self._sigma/self._a) * (1-exp(-self._a*t)))**2
   
    def alpha0(self,t):
        return (self.r0 -self.f_M(0))#*exp(-self._a*t)
    def y0(self):
        return self.f_M(0)
        
        
            
class EulerScheme:
    def __init__(self,process):
        self._process = process
       
    def dW(self,dt):
        return np.random.normal(0,dt)
    
    def integrate(self,num_sims,npoints,t0,tn):
        dt     = float(tn - t0) / npoints
        ts    = np.arange(t0, tn, dt)
        xs    = np.zeros((num_sims,npoints))
        ys    = np.zeros((num_sims,npoints))
        
        ys[:,0] = self._process.r0*np.ones((num_sims,))
        alphas = [self._process.alpha(i*dt)  for i in range(1,ts.size)] 
        dst = sqrt(dt)
        for j in range(num_sims):
            for i in range(1, ts.size):
                t = i* dt
                x = xs[j,i-1]
                xs[j,i] =  x + self._process.mu(x, t) * dt + self._process.sigma(x, t) * self.dW(dst) 
                ys[j,i] =  alphas[i-1] + xs[j,i] 
        return ts,ys
    
irc = convfft.IRCurve(datetime.datetime.now(),1,'Y')
irc.set(1, 0.05)
irc.set(4, 0.06)
irc.set(6, 0.04)
irc.set(8, 0.04)
irc.set(10,0.04)
irc.set(15, 0.03)
irc.set(20,0.02)

A=0.3
SIGMA=0.05
hw1 = HWProcess(A,SIGMA,irc.zcp,0.05)
sch = EulerScheme(hw1)

ts, ys = sch.integrate(1000,360,0,30,)
hw2 = convfft.HullWhite(A,SIGMA,irc,0.05)

v_ = np.sqrt(np.var(ys,axis=0))
vv = np.sqrt(np.vectorize(hw2.varHW)(ts))

#plt.plot(ts,v_,'r-.',label='Volat MC')
#plt.plot(ts,vv,'g-.',label='Analytic')
#plt.legend()
plt.show()

y_ = np.mean(ys,axis=0)
#yy = np.vectorize(hw1.r_)(ts)
fm = np.vectorize(hw1.f_M)(ts)
plt.plot(ts,y_,'r-.',label='Expectation MC')

plt.plot(ts,fm,'g-.',label='Forward')




for i in range(0,10):
    plt.plot(ts,ys[i,:])
plt.legend()    
plt.show()

